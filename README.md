# How to fix issues with Wifi on the PineTab 2:

The Wifi should work out of the box if your device is using the most recent [Factory Image](https://pine64.org/documentation/PineTab2/Software/Releases/#factory_releases).

If the Wifi is not working for you, follow one of the following steps:

## 1. If you don't mind losing everything on your PineTab and want to start over
Flash the latest [Factory Image](https://pine64.org/documentation/PineTab2/Software/Releases/#factory_releases) to your PineTab. 

It enables the Wifi driver by default.

## 2. If you want to keep your existing OS install
If you want to keep currently installed programs and files, perform one of the following steps to update your PineTab:

### A) Manually enable the wifi and update (more recent kernels only)
Open the terminal (Konsole). If `uname -a` gives Kernel version `6.6.13-danctnix1` or newer, you can run `sudo modprobe bes2600`. This enables Wifi for one session, allowing you to connect and perform the update. When connected, run `sudo pacman -Syu` to update the system and then reboot. 

### B) Connect a supported USB wifi dongle and update (any kernel version)
Connect the USB dongle, connect to your network and run `sudo pacman -Syu` to update the system, and then reboot. When unplugging the dongle, you will have to enter the password for your Wifi network again.


# Development of the Wifi driver

Development is not taking place here. 
This repo was initially forked from [Segfault / bes2600](https://gitlab.com/TuxThePenguin0/bes2600), 
but since neither me nor Segfault are doing development on bes2600, the codebase quickly got outdated.

Since then, this repo mainly exists to provide compilation and installation instructions.
As those are no longer needed, I've removed the source code from the main branch. 

There are others in the community who have done more work on the driver and have up to date copies
of the sources online. A good choice to get started would be [https://github.com/cringeops/bes2600](https://github.com/cringeops/bes2600) (not my work)

# Manually compiling the driver (OLD)
If you still want to read the old instructions to manually compile and enable
the bes2600 driver, for example on older kernel versions, you can find these, along with the sources needed, here:

[OLD_README.md](https://gitlab.com/arjanvlek/bes2600/-/blob/test/OLD_README.md)